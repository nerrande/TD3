/**
 *
 * @param a
 * @param b
 * @return le max entre a et b
 */
int max2(int a, int b ) {
    if (a > b) { return a; }
    else       { return b; }

}
/**
 *
 * @param a
 * @param b
 * @param c
 * @return le max entre a, b et c
 */
int max3(int a, int b, int c){
    return max2(max2(a,b),c);
}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture, en base 10, de n
 */
int nbChiffres (int n  ) {
    int somme =1;
    while (n>=10){
        n=n-n%10;
        n=n/10;
        somme+=1;
    }
    return somme;
}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture du nombre n^2
 */

int nbChiffresDuCarre(int n){
    return nbChiffres(n*n);
}

/**
 * afficher nb caractères car (à partir de la position courante du curseur sur le terminal).
 * @param nb
 * @param car
 */
void  repeteCarac(int nb, char car){
    for (int i = 1; i <= nb; i++) {
        Ut.afficher(car);
    }

}

/**
 * affiche à l'écran une pyramide de hauteur h constituée de lignes répétant le caractère c.
 * @param h
 * @param c
 */
void pyramideSimple(int h, char c){
    h = 2*h-1;
    for (int i = 1; i <= h; i+=2) {
        for (int j = 1; j <= (h-i)/2; j++) {
            Ut.afficher("  ");}
        repeteCarac(i,c);

        for (int k = 1; k <= (h-i)/2; k++) {
            Ut.afficher("  ");}


        Ut.afficherSL("");
    }

}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre croissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresCroissants (int nb1,int nb2 ){
    while (nb1 <= nb2) {
        int unite = nb1%10;
        Ut.afficher(unite + " ");


        nb1+=1;
    }

}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre décroissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresDecroissants (int nb1,int nb2 ){
    while (nb1 <= nb2) {
        int unite = nb2%10;
        Ut.afficher(unite + " ");
        nb2-=1;
    }

}

/**
 * permet de représenter à l'écran la pyramide
 * @param h
 */

void pyramide(int h ) {
    for (int i = 1; i <= h; i++) {

        for (int j = 1; j <= h-i; j++) {
            Ut.afficher("  ");
        }
        afficheNombresCroissants(i,2*i-1);
        afficheNombresDecroissants( i , 2*i-2);
        Ut.afficherSL("");
    }

}

/**
 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonctio * Sinon la fonction retourne -1.
n retourne -1.
 */
int racineParfaite(int c ){
    int carre=0;
    int i =0;
    while (carre<c){
        i+=1;
        carre=i*i;
    }
    if (carre==c){
        return i;
    }
    else {
        return -1;
    }

}

/**
 *
 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb){
    return racineParfaite(nb)!=-1;

}

/**
 *
 * @param p
 * @param q
 * @return vrai  si deux nombres entiers `p`et `q` sont amis, faux sinon.
 */
boolean nombresAmicaux(int p , int q){
    int sommep = 0;
    for (int i = 1; i < p; i++) {
        if (p%i==0){
            sommep+=i;
        }}

    int sommeq = 0;
    for (int j = 1; j < q; j++) {
        if (q%j==0){
            sommeq+=j;
        }
    }

    return (sommeq==p && sommep==q);
}



/**
 * affiche à l'écran les couples de
 *nombres amis parmi les nombres inférieurs ou égaux à max.
 * @param max
 * PR : max entier non nul
 */

void afficheAmicaux(int max){
    for (int i = 1; i <= max; i++) {
        for (int j = 1; j <= max-i; j++) {
            if (nombresAmicaux(i,j)){
                Ut.afficherSL(i + "   " + j);
            }

        }
    }
}

/**
 * @param c1
 * @param c2
 * @return vrai si deux entiers donnés peuvent  être les côtés de l'angle droit d'un triangle rectangle dont les 3
 *        côtés sont des nombres entiers, faux sinon
 */
boolean  estTriangleRectangle (int c1, int c2) {
    int sommeCarree = c1 * c1 + c2 * c2;
    int i = 1;
    while (i * i < sommeCarree) {
        i += 1;
    }
    return i * i == sommeCarree;
}

/**Action : affiche les couples de nombres entiers qui peuvent etre les cotes de l'angle d'un triangle rectange,
 * affiche tout les couples jusqua ce que le perimetre du triangle soit superieur au perimetre choisi
 *
 * @param perimetre
 */
void afficheCoupleTriangleRectangle(int perimetre){
    int b=1;
    int a=1;
    int c= 0;
    while (a+b<=perimetre){

        while (a+b<=perimetre){
            if (estTriangleRectangle(a,b)){
                //c = perimetre-a-b;
                //if(c*c==a*a+b*b)
                Ut.afficherSL(a+"  "+b);
                }


            a+=1;
            }
        b+=1;
        a=1;
        }




    }

boolean nombreSyracusien(int n, int nbMaxOp){
    int i=0;
    while (i <= nbMaxOp){
        if (n==1){return true;}
        else if(n%2==0){n=n/2;}
        else {n=3*n +1;}
    }
    return false;
}

/**
*Action : calcule recursivement e
 * @param n
 * @param denominateur
 *
 * PR : denominateur entier positif non nul
* PR: n entier positif
 */
double calculRecursifE(int n, int denominateur){
    if (n==0){return 0;}
    else{
    if (denominateur%2==0){
        return 1/(2+calculRecursifE(n-1,denominateur+1));
    }
    else {
        return 1/(denominateur-calculRecursifE(n-1,denominateur+1));
    }



    }



}

double calculE(int n){
    return 1+calculRecursifE(n,1);
}


int generateurNombreAleatoire(int min, int max){

    return Ut.randomMinMax(min, max);

}
/* Action : simule la trajectoire d'un marin ivre sur la planche
* Retourne : vrai si le marin atteint le bateau
*PR: longueur > largeur
*PR: largeur est impair
*PR: longueur et largeurs deux entiers positifs
 */
boolean arivobato(int longueur, int largeur) {
    int posX = 0;
    int posY = (largeur - 1) / 2;
    boolean tombe = false;
    boolean arrive = false;
    while (tombe == false && arrive == false) {
        int n = generateurNombreAleatoire(1, 100);
        if (n <= 50) {
            posX++;
            if (posX > longueur) {
                arrive = true;
            }
        } else if (n <= 70) {
            posY = posY - 1;
            if (posY < 0) {
                tombe = true;
            }
        } else if (n <= 90) {
            posY = posY + 1;
            if (posY > largeur) {
                tombe = true;
            }
        } else {
            posX = posX - 1;
            if (posX < 0) {
                posX = 0;
                posY = (largeur - 1) / 2;
            }
        }
    }
    if (arrive) {
        return true;
    }
    return false;
}


void affichePlanche(int longueur, int largeur) {
    //for (int x)


}

void main (){
    Ut.afficher(arivobato(8,3));
}


